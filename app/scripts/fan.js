window.mapesa_fan = (function($){
	"use strict"
	var init,
		open,
		close,
		toggle,
		calculate_total_width,
		calculate_item_width,
		set_item_width,
		events,
		defaults = {},
		config = {},
		fan_wrapper = null,
		items = null, 
		item_width = 0,
		total_width = 0;

	defaults = {
		dom: {
			fan: "#index_maps",
			items: '.js_map_item'
		},
		final_width: 322,
		final_height: 167,
		gutter_width: 1,
		wrapper_class: "fan_wrapper"
	}

	init = function( options ){
		var fan = null;
		config = $.extend({}, defaults, options);
		if($(config.dom.fan).length === 0){
			return;
		}
		fan = $(config.dom.fan);
		fan_wrapper = document.createElement( 'div' );
		fan_wrapper.className = config.wrapper_class;
		fan.wrap(fan_wrapper);
		items = fan.find(config.dom.items);
		total_width = calculate_total_width(fan);
		item_width = calculate_item_width();
		set_item_width();
		events();
		open(items.first());
	}

	open = function(item){
		if(!item.hasClass("fan_opened")){
			set_item_width();
			item.css("width", config.final_width + "px").addClass("fan_opened");
			item.removeClass("fan_closed");
		}
	}
	
	close = function(){
		
	}

	toggle = function(){

	}

	calculate_total_width = function(fan){
		return $(fan).width();
	}

	calculate_item_width = function(){
		var total_items = items.length,
			width = 0;
			width = ((config.final_width / 3) - config.gutter_width) | 0;
		return width
	}

	set_item_width = function(){
		items.each(function(index, item){
			$(item).css({
				width: item_width + "px",
				"margin-right": config.gutter_width + "px",
				"height": config.final_height + "px",
				float: "left"
			}).removeClass("fan_opened").addClass('fan_closed');
		});
	}

	events = function(){
		$(config.dom.fan).on("click", config.dom.items, function(){
			open($(this));
		});
	}

	return {
		init: function(){
			init();
		},

		open: function(item){
			open(item);
		}
	}
})(jQuery);